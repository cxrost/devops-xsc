Task1 - Build Requirements

| Требование/название библиотеки  | Минимальное требование/номер версии | Как установить |
| ------------- | ------------- | ------------- |
| ОС Ubuntu  | 18.04  | https://help.ubuntu.ru/wiki/ubuntu_install  |
| pip  |  21.0.1 | sudo apt install python3-pip  |
|numpy   |  1.20.1 | pip install numpy  |
|scipy |  1.3.1 | pip install scipy  |
|torch   | 1.8.0  | pip install torch  |
|torchvision   |  0.9.0 | pip install torchvision  |
|...   |   |   |
|requirments.txt  |   |  pip install -r requirements.txt |
